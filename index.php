<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="UTF-8">
    <!-- meta responsável por ajustar os width = ao tamanho de tela dos aparelhos -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
    <title>Login</title>
    <link href="css/estilo.css" type="text/css" rel="stylesheet"/>
    <link href="css/estilo_login.css" type="text/css" rel="stylesheet"/>
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
  </head>
    <body style="background-color: #ccc;">
        <script>
            $(document).ready(function () {
                $('#myModal').modal('show');
            });
        </script>  
        <!-- Modal -->
        <div id="myModal" class="modal fade" role="dialog">
            <div class="modal-dialog">

                Modal content
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">
                            <img src="img/firefox.jpeg" title="firefox" alt="firefox" width="30px" height="30px"/>
                        </h4>
                    </div>
                    <div class="modal-body">
                        Para uma melhor experiência, utilize o Mozilla Firefox como seu navegador padrão
                        na sua versão mais atualizada!!!

                    </div>
                    <br/>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
        <!--fim do modal-->



        <div class="modal-dialog">
            <div class="loginmodal-container">
                <form action="" method="post">

                    <h1 style="font-family: 'Oswald', sans-serif;">Entre com sua conta</h1><br>

                    <input text-capitalize type="text" name="idt" pattern=["0-9"]+$ maxlength="10" required placeholder="Identidade Militar (somente números)">
                    <input type="password" name="senha" required placeholder="Senha">

                    <input type="hidden" name="acao" value="logar"/>
                    <input type="submit" class="login loginmodal-submit" value="Login">
                </form>

                <div class="login-help">
                    <a href="register.php">Criar nova conta</a> - <a data-toggle="modal" href="#esqueciSenha">Esqueci a senha</a>
                </div>
                <!--===============================================-->
                <!--MODAL PARA ALERTA DE ESQUECI A SENHA-->
                <div class="modal fade" id="esqueciSenha" role="dialog">
                    <div class="modal-dialog">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Aviso importante!</h4>
                            </div>
                            <div class="modal-body">
                                <p>Entre em contato com o Setor Cultural.</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                <!--============================================-->
            </div>
        </div>

       <!-- Interessante carregar sempre o javascript por último, por questão de performance do site -->
       <script type="text/javascript" src="js/jquery2.js"></script>
       <script type="text/javascript" src="js/bootstrap-dropdown.js"></script>
       <?php include 'includes/footer.php'; ?>
    </body>

    <style>
        /*@import url(http://fonts.googleapis.com/css?family=Roboto);*/

        /****** LOGIN MODAL ******/
        .loginmodal-container {
            padding: 30px;
            max-width: 400px;
            width: 100% !important;
            background-color: #F7F7F7;
            margin: 0 auto;
            border-radius: 2px;
            box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
            overflow: hidden;
            font-family: roboto;
        }

        .loginmodal-container h1 {
            text-align: center;
            font-size: 1.8em;
            font-family: roboto;
        }

        .loginmodal-container input[type=submit] {
            width: 100%;
            display: block;
            margin-bottom: 10px;
            position: relative;
        }

        .loginmodal-container input[type=text], input[type=password] {
            height: 44px;
            font-size: 14px;
            width: 100%;
            margin-bottom: 10px;
            -webkit-appearance: none;
            background: #fff;
            border: 1px solid #d9d9d9;
            border-top: 1px solid #c0c0c0;
            /* border-radius: 2px; */
            padding: 0 8px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        .loginmodal-container input[type=text]:hover, input[type=password]:hover {
            border: 1px solid #b9b9b9;
            border-top: 1px solid #a0a0a0;
            -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
            -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
            box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
        }

        .loginmodal {
            text-align: center;
            font-size: 14px;
            font-family: 'Arial', sans-serif;
            font-weight: 700;
            height: 36px;
            padding: 0 8px;
            /* border-radius: 3px; */
            /* -webkit-user-select: none;
              user-select: none; */
        }

        .loginmodal-submit {
            /* border: 1px solid #3079ed; */
            border: 0px;
            color: #fff;
            text-shadow: 0 1px rgba(0,0,0,0.1); 
            background-color: #4d90fe;
            padding: 17px 0px;
            font-family: roboto;
            font-size: 14px;
            /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
        }

        .loginmodal-submit:hover {
            /* border: 1px solid #2f5bb7; */
            border: 0px;
            text-shadow: 0 1px rgba(0,0,0,0.3);
            background-color: #357ae8;
            /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
        }

        .loginmodal-container a {
            text-decoration: none;
            color: #666;
            font-weight: 400;
            text-align: center;
            display: inline-block;
            opacity: 0.6;
            transition: opacity ease 0.5s;
        } 

        .login-help{
            font-size: 12px;
        }
    </style>
</html>
